
Integrantes del equipo:

-Francisco Daniel Gamez Garcia
-Luis Gerardo Bernabe Gomez
-Brandon Padilla Ruiz

Prerrequisitos

-Postgres 9.5.10
-Python 2.7
-pip 9.0.1

Instalaci�n
Navegamos al directorio del repositorio y se deber�n ejecutar los siguientes comandos:

Para instalar los paquetes de python necesarios se utiliza el siguiente comando:

pip install -r requirements.txt

Para cargar esquema de base de datos:

sudo -u postgres psql postgres -f ./src/db_stuff/schema.sql

Para cargar datos de prueba a la base de datos:

python -m src.db_stuff.initialize_db

Ejecuci�n del programa

Desde el directorio principal del repositorio, ejecutamos los siguientes comandos.

Para ejecutar servidor:

python -m src.controller.server

Para ejecutar cliente (aplicaci�n):

python -m src.view.interface


Nota: El archivo del cliente interface.py acepta <ip_address, port> argumentos de l�nea de comando.

Makefile

Adem�s, puede ejecutar los comandos anteriores a trav�s del archivo MAKE incluido.

Instalar los paquetes de python necesarios:

make install-requirements

Inicializar servidor:

make install-server

Generar documentaci�n:

make generate-doc

Ejecutar servidor:

make run-server

Ejecutar cliente:

make run-client

